const mysql = require("mysql2");

const db = mysql.createPool({
    connectionLimit: 5,
    host: "localhost",
    user: "root",
    database: "its_report",
    password: "password"
});

module.exports = db; 