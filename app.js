const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const api = require("./api");
 
const app = express();
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', api);

app.listen(3000, function () {
  console.log("Сервер ожидает подключения...");
});

module.exports.app = app; 