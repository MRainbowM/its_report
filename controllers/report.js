const db = require("../db");

const getAll = (req, res) => {
    db.query(`select reports.id AS 'reports_id', reports.date as 'reports_date',
    reports.comment as 'reports_comment', reports.duration as 'reports_duration',
    reports.state as 'reports_state', reports.created_at as 'reports_created_at',
    reports.id_user as 'users_id', users.surname as 'users_surname',
    users.name as 'users_name', users.m_name as 'users_m_name',
    reports.id_project as 'projects_id',
    projects.name as 'projects_name' from reports
    inner join users on reports.id_user = users.id
    inner join projects on reports.id_project = projects.id;`, 
    function (err, data) {
        if (err) return console.log(err);
        res.send({ data })
    });
};

const create = (req, res) => {
    if (!req.body) return res.sendStatus(400);
    const date = req.body.date;
    const id_user = req.body.id_user;
    const id_project = req.body.id_project;
    const comment = req.body.comment;
    const duration = req.body.duration;
    const state = +1;
    db.query("INSERT INTO reports (date, id_user, id_project, comment, duration, state) VALUES (?,?,?,?,?,?)", 
    [date, id_user, id_project, comment, duration, state], 
    function (err, data) {
        if (err) return console.log(err);
        res.send({ data });
    });
};

const getById = (req, res) => {
    const id = req.params.id;
    console.log({ id })
    db.query("SELECT * FROM reports WHERE id=?", [id], function (err, data) {
        if (err) return console.log(err);
        res.send({ data: data[0] })
    });
};

const update = (req, res) => {
    if (!req.body) return res.sendStatus(400);
    const date = req.body.date;
    const id_project = req.body.duration;
    const comment = req.body.comment;
    const duration = req.body.duration;
    const id = req.params.id;

    db.query("UPDATE reports SET date=?, id_project=?, comment=?, duration=? WHERE id=?", 
    [date, id_project, comment, duration, id], 
    function (err, data) {
        if (err) return console.log(err);
        res.send({ data });
    });
};

const remove = (req, res) => {
    if (!req.body) return res.sendStatus(400);
    const id = req.params.id;

    db.query("UPDATE reports SET state=0 WHERE id=?", 
    [id], 
    function (err, data) {
        if (err) return console.log(err);
        res.send({ data });
    });
};

module.exports.getAll = getAll;
module.exports.create = create;
module.exports.getById = getById;
module.exports.update = update;
module.exports.remove = remove;