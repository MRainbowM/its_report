const db = require("../db");

const getAll = (req, res) => {
  db.query("SELECT * FROM projects", function (err, data) {
    if (err) return console.log(err);
    res.send({ data })
  });
};

const create = (req, res) => {
  if (!req.body) return res.sendStatus(400);
  const name = req.body.name;
  const state = +1;
  db.query("INSERT INTO projects (name, state) VALUES (?,?)", [name, state], function (err, data) {
    if (err) return console.log(err);
    res.send({ data });
  });
};

const getById = (req, res) => {
  const id = req.params.id;
  console.log({ id })
  db.query("SELECT * FROM projects WHERE id=?", [id], function (err, data) {
    if (err) return console.log(err);
    res.send({ data: data[0] })
  });
};

const update = (req, res) => {
  if (!req.body) return res.sendStatus(400);
  const name = req.body.name;
  const id = req.params.id;

  db.query("UPDATE projects SET name=?, WHERE id=?", [name, id], function (err, data) {
    if (err) return console.log(err);
    res.send({ data });
  });
};

const remove = (req, res) => {
  if (!req.body) return res.sendStatus(400);
  const id = req.paramss.id;

  db.query("UPDATE projects SET state=0 WHERE id=?", [id], function (err, data) {
    if (err) return console.log(err);
    res.send({ data });
  });
};

module.exports.getAll = getAll;
module.exports.create = create;
module.exports.getById = getById;
module.exports.update = update;
module.exports.remove = remove;