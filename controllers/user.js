const db = require("../db");

const getAll = (req, res) => {
  db.query("SELECT * FROM users", function (err, data) {
    if (err) return console.log(err);
    res.send({ data })
  });
};

const create = (req, res) => {
  if (!req.body) return res.sendStatus(400);
  const surname = req.body.surname;
  const name = req.body.name;
  const m_name = req.body.m_name;
  const login = req.body.login;
  const password = req.body.password;
  const role = req.body.role;
  const state = +1; //1-существует, 0-удален
  db.query("INSERT INTO users (surname, name, m_name, login, password, role, state) VALUES (?,?,?,?,?,?,?)", [surname, name, m_name, login, password, role, state], function (err, data) {
    if (err) return console.log(err);
    res.send({ data });
  });
};

const getById = (req, res) => {
  const id = req.params.id;
  console.log({ id })
  db.query("SELECT * FROM users WHERE id=?", [id], function (err, data) {
    if (err) return console.log(err);
    console.log(data)
    res.send({ data: data[0] })
  });
};

const update = (req, res) => {
  if (!req.body) return res.sendStatus(400);
  const surname = req.body.surname;
  const name = req.body.name;
  const m_name = req.body.m_name;
  const login = req.body.login;
  const password = req.body.password;
  const role = req.body.role;
  const id = req.params.id;

  db.query("UPDATE users SET surname=?, name=?, m_name=?, login=?, password=?, role=?, state=? WHERE id=?", [surname, name, m_name, login, password, role, state, id], function (err, data) {
    if (err) return console.log(err);
    res.send({ data });
  });
};

const remove = (req, res) => {
  if (!req.body) return res.sendStatus(400);
  const id = req.params.id;

  db.query("UPDATE users SET state=0 WHERE id=?", [id], function (err, data) {
    if (err) return console.log(err);
    res.send({ data });
  });
};

module.exports.getAll = getAll;
module.exports.create = create;
module.exports.getById = getById;
module.exports.update = update;
module.exports.remove = remove;