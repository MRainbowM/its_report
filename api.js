const express = require('express');
const api = express.Router();

const UserControler = require("./controllers/user");
const ProjectControler = require("./controllers/project");
const ReportControler = require("./controllers/report");

api.get("/users", UserControler.getAll);
api.post("/user", UserControler.create);
api.get("/user/:id", UserControler.getById);
api.post("/user/:id", UserControler.update);
api.delete("/user/:id", UserControler.remove);

api.get("/projects", ProjectControler.getAll);
api.post("/project", ProjectControler.create);
api.get("/project/:id", ProjectControler.getById);
api.post("/project/:id", ProjectControler.update);
api.delete("/project/:id", ProjectControler.remove);

api.get("/reports", ReportControler.getAll);
api.post("/report", ReportControler.create);
api.get("/report/:id", ReportControler.getById);
api.post("/report/:id", ReportControler.update);
api.delete("/report/:id", ReportControler.remove);

module.exports = api;